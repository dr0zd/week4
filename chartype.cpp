#include <iostream>
using namespace std;

int main() {

  char character;
  
  cout << "Enter a character to be checked: ";
  cin >> character;
  
  if (isdigit(character)) {
    cout << character << " is a digit." << endl;
  } else if (ispunct(character)) {
    cout << character << " is a punctuation." << endl;
  } else if (character == 'a' || character == 'e' || character == 'i' ||
	     character == 'o' || character == 'u') {
    cout << character << " is a vowel. " << endl;
  } else if (character == 'b' || character == 'c' || character == 'd' ||
	     character == 'f' || character == 'g' || character == 'h' ||
	     character == 'j' || character == 'k' || character == 'l' ||
	     character == 'm' || character == 'n' || character == 'p' ||
	     character == 'q' || character == 'r' || character == 's' ||
	     character == 't' || character == 'v' || character == 'w' ||
	     character == 'x' || character == 'z') {
    cout << character << " is a consonant. " << endl;
  } else
    cout << character << " is undefined." << endl;
  
  return 0;
}
